#pragma once

#include "RGA/RGA.h"
#include <map>
#define MYSQLPP_MYSQL_HEADERS_BURIED
#include <mysql++/mysql++.h>
#include <thread>
#include <fstream>
#include <optional>
#include <vector>
#include <string>

namespace RB {
    class GameSubsystem{
    private:
        static std::map<std::string, RGA::Game> games;
        static std::map<long long int, RGA::GameInstance> instances;
        static bool dbInfoLoaded;
        static std::string dbHost;
        static std::string dbUser;
        static std::string dbPassword;
        static mysqlpp::Connection db;
        static void loadDBInfo();
        static std::optional<RGA::GameInstance*> getInstance(const long long int &chatID);
        static void deleteDBInstance(const long long int &chatID);
    public:
        static void startGame(RGA::Game *game, const long long int &chatID);
        static bool inGame(const long long int &chatID);
        static void stopGame(const long long int &chatID);
        static void connectDB();
        static void loadGames();
        static std::optional<RGA::Game *>getInstanceGame(const long long int &chatID);
        static RGA::Game *getGameByID(const std::string &gameID);
        static std::vector<RGA::Game *> getGamesList();
        static const std::string &viewInstanceText(const long long int &chatid);
    };
}