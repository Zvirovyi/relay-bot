#include "RelayBot.h"

namespace RB {
    //Variables
    std::map<std::string, RGA::Game> GameSubsystem::games;
    std::map<long long int, RGA::GameInstance> GameSubsystem::instances;
    bool GameSubsystem::dbInfoLoaded = false;
    std::string GameSubsystem::dbHost;
    std::string GameSubsystem::dbUser;
    std::string GameSubsystem::dbPassword;
    mysqlpp::Connection GameSubsystem::db;


    void GameSubsystem::startGame(RGA::Game *game, const long long int &chatID) {
        instances.try_emplace(chatID, game, game->getStartBlock(), game->viewBoolVars(), game->viewIntVars());
        mysqlpp::Query insertInstance = db.query("insert into GameInstances values(%0,%1q,%2q,%3);");
        insertInstance.parse();
        insertInstance.execute(chatID, "Sobaka", game->getStartBlock()->getID(), game->viewVersion());
        mysqlpp::Query insertVars = db.query("insert into GameVars values(%0,%1q,%2,%3);");
        insertVars.parse();
        for (const auto &boolVar: game->viewBoolVars()) {
            insertVars.execute(chatID, boolVar.first, boolVar.second, mysqlpp::null);
        }
        for (const auto &intVar: game->viewIntVars()) {
            insertVars.execute(chatID, intVar.first, mysqlpp::null, intVar.second);
        }
    }


    bool GameSubsystem::inGame(const long long int &chatID) {
        std::optional<RGA::GameInstance *> instance = getInstance(chatID);
        if (instance.has_value() && instance.value() != nullptr) {
            return true;
        } else {
            return false;
        }
    }


    void GameSubsystem::stopGame(const long long int &chatID) {
        deleteDBInstance(chatID);
        instances.erase(chatID);
    }


    //Other
    void GameSubsystem::connectDB() {
        if (!dbInfoLoaded) {
            loadDBInfo();
        }
        while (true) {
            try{
                db.connect("RelayBot",dbHost.c_str(), dbUser.c_str(), dbPassword.c_str());
                break;
            } catch (const mysqlpp::ConnectionFailed &e) {
                std::cout << "DB connection error" << std::endl;
                std::this_thread::sleep_for(std::chrono::minutes(1));
            }
        }
    }


    void GameSubsystem::loadDBInfo() {
        std::fstream file("db", std::ios::in | std::ios::binary);
        std::getline(file, dbHost);
        std::getline(file, dbUser);
        std::getline(file, dbPassword);
        file.close();
        dbInfoLoaded = true;
    }


    std::optional<RGA::GameInstance*> GameSubsystem::getInstance(const long long int &chatID) {
        if (instances.find(chatID) != instances.end()) {
            return &instances.at(chatID);
        } else {
            mysqlpp::Query instanceQuery = db.query("select GameID, GameBlockID, GameVersion from GameInstances where ChatID = %0 limit 1;");
            instanceQuery.parse();
            mysqlpp::StoreQueryResult instanceResult;
            instanceResult = instanceQuery.store(chatID);
            if (instanceResult.empty()) {
                return std::nullopt;
            } else {
                RGA::Game *instanceGame = getGameByID((std::string)instanceResult[0]["GameID"]);
                if (instanceGame == nullptr || instanceGame->viewVersion() != (short int)instanceResult[0]["GameVersion"]) {
                    deleteDBInstance(chatID);
                    return nullptr;
                } else {
                    std::map<std::string, bool> instanceBoolVars;
                    std::map<std::string, short int> instanceIntVars;
                    mysqlpp::Query varsQuery = db.query("select VarName, VarBool, VarInt from GameVars where ChatID = %0;");
                    varsQuery.parse();
                    mysqlpp::StoreQueryResult varsQueryResult = varsQuery.store(chatID);
                    for (const auto &var: varsQueryResult) {
                        if (var["VarBool"].is_null()) {
                            instanceIntVars.emplace(var["VarName"],var["VarInt"]);
                        } else {
                            instanceBoolVars.emplace(var["VarName"], var["VarBool"]);
                        }
                    }
                    instances.try_emplace(chatID, instanceGame, instanceGame->getGameBlockByID((std::string)instanceResult[0]["GameBlockID"]), instanceBoolVars, instanceIntVars);
                    return &instances.at(chatID);
                }
            }
        }
    }


    void GameSubsystem::loadGames() {
        std::filesystem::path gamesDir("Games");
        for (const auto &gameDir: std::filesystem::directory_iterator(gamesDir)) {
            std::filesystem::path rgiFile = gameDir.path() / gameDir.path().filename().replace_extension(".rgi");
            std::filesystem::path rgvFile = gameDir.path() / gameDir.path().filename().replace_extension(".rgv");
            std::filesystem::path rgdFile = gameDir.path() / gameDir.path().filename().replace_extension(".rgd");
            if (gameDir.is_directory() && std::filesystem::exists(rgiFile) && std::filesystem::exists(rgvFile) && std::filesystem::exists(rgdFile)) {
                std::cout << "Game to load: " << gameDir.path().filename() << std::endl;
                bool emplaced = false;
                try{
                    games.try_emplace(gameDir.path().filename(), gameDir.path().filename(), rgiFile);
                    emplaced = true;
                    games.at(gameDir.path().filename()).loadFromFile(rgvFile, rgdFile);
                    std::cout << "Game loaded" << std::endl;
                } catch (const std::runtime_error &e) {
                    if (emplaced) games.erase(gameDir.path().filename());
                    std::cout << "Error during game loading: " <<  e.what() << std::endl << "Game was skipped" << std::endl;
                }
            }
        }
    }


    //Nullptr - error. Nullopt - nothing
    std::optional<RGA::Game *>GameSubsystem::getInstanceGame(const long long int &chatID) {
        std::optional<RGA::GameInstance*> instance = getInstance(chatID);
        if (!instance.has_value()) {
            return std::nullopt;
        } else {
            return instance.value() != nullptr ? instance.value()->getGame() : nullptr;
        }
    }


    RGA::Game *GameSubsystem::getGameByID(const std::string &gameID) {
        for (auto &game: games) {
            if (game.second.viewID() == gameID) {
                return &game.second;
            }
        }
        return nullptr;
    }


    void GameSubsystem::deleteDBInstance(const long long int &chatID) {
        mysqlpp::Query query = db.query("delete from GameInstances where ChatID = %0 limit 1;");
        query.parse();
        query.execute(chatID);
    }


    std::vector<RGA::Game *> GameSubsystem::getGamesList() {
        std::vector<RGA::Game *> res;
        for (auto &game: games) {
            res.emplace_back(&game.second);
        }
        return res;
    }


    const std::string &GameSubsystem::viewInstanceText(const long long int &chatID) {
        std::optional<RGA::GameInstance*> instance = getInstance(chatID);
        return instance.value()->viewCurrentBlockText();
    }
}