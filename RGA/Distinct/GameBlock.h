#pragma once

#include "GameBlockChoice.h"
#include <vector>
#include <string>
#include <map>
#include <functional>

namespace RGA {
    class GameBlockChoice;

    class GameBlock{
    private:
        std::string id;
        std::string text;
        std::vector<GameBlockChoice> choices;
        std::optional<std::function<void(std::map<std::string, bool> &boolVars, std::map<std::string, short int> &intVars)>> action;
    public:
        GameBlock(const std::string &id, const std::string &text, const std::optional<std::function<void(std::map<std::string, bool> &boolVars, std::map<std::string, short int> &intVars)>> &action = std::nullopt);

        template <class ...Args>
        void emplaceChoice(Args&&... args);

        [[nodiscard]]
        std::string getText() const;

        [[nodiscard]]
        const std::string &viewText() const;

        [[nodiscard]]
        std::vector<GameBlockChoice> getChoices() const;

        [[nodiscard]]
        const std::vector<GameBlockChoice> &viewChoices() const;

        [[nodiscard]]
        std::string getID() const;

        [[nodiscard]]
        const std::string &viewID() const;

        void execAction(std::map<std::string, bool> &boolVars, std::map<std::string, short int> &intVars);

        [[nodiscard]]
        bool hasAction();
    };

    #include "GameBlock.tpp"
}