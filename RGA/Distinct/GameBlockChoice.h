#pragma once

#include "GameBlock.h"
#include <string>
#include <map>
#include <functional>

namespace RGA {
    class GameBlock;

    class GameBlockChoice{
    private:
        std::string text;
        GameBlock *block;
        std::optional<std::function<bool(const std::map<std::string, bool> &boolVars, const std::map<std::string, short int> &intVars)>> condition;
    public:
        GameBlockChoice(GameBlock *block, const std::string &text, const std::optional<std::function<bool(const std::map<std::string, bool> &boolVars, const std::map<std::string, short int> &intVars)>> &condition = std::nullopt);

        [[nodiscard]]
        std::string getText() const;

        [[nodiscard]]
        const std::string &viewText() const;

        [[nodiscard]]
        bool isValid(const std::map<std::string, bool> &boolVars, const std::map<std::string, short int> &intVars) const;

        [[nodiscard]]
        GameBlock *getBlock() const;
    };
}