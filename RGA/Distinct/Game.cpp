#include "Game.h"

namespace RGA {
    Game::Game(const std::string &id, const short int &version, const std::string &name, const std::string &description, const std::string &author) {
        this->id = id;
        this->version = version;
        this->name = name;
        this->description = description;
        this->author = author;
    }

    Game::Game(const std::string &id, const std::filesystem::path &rgi) {
        this->id = id;
        std::fstream file(rgi, std::ios::in | std::ios::binary);
        char ch;
        std::string prefix;
        std::string postfix;
        //0 - prefix, 1 - postfix
        bool pos = false;
        //For prefix
        bool completed = false;
        //For postfix
        bool input = false;
        while (file.get(ch)) {
            switch (ch) {
                case ' ': {
                    if (!pos) {
                        if (!prefix.empty()) {
                            completed = true;
                        }
                    } else if (input) {
                        postfix += ch;
                    }
                } break;
                case ':': {
                    if (!pos) {
                        if (prefix.empty()) {
                            file.close();
                            throw std::runtime_error("[rgi] prefix not specified");
                        }
                        completed = false;
                        pos = true;
                    } else {
                        file.close();
                        throw std::runtime_error("[rgi] double ':'");
                    }
                } break;
                case '\n': {
                    if (!pos) {
                        if (!prefix.empty()) {
                            file.close();
                            throw std::runtime_error("[rgi] postfix not specified");
                        }
                    } else {
                        while (!postfix.empty() && postfix[postfix.size() - 1] == ' ') {
                            postfix = postfix.substr(0, postfix.size() - 1);
                        }
                        if (postfix.empty()) {
                            file.close();
                            throw std::runtime_error("[rgi] postfix not specified");
                        } else {
                            if (prefix == "name") {
                                name = postfix;
                            } else if (prefix == "description") {
                                description = postfix;
                            } else if (prefix == "version") {
                                try {
                                    version = std::stoi(postfix);
                                } catch (...) {
                                    file.close();
                                    throw std::runtime_error("[rgi] version postfix in't int");
                                }
                            } else if (prefix == "author") {
                                author = postfix;
                            } else {
                                file.close();
                                throw std::runtime_error("[rgi] unfamiliar prefix");
                            }
                            pos = false;
                            input = false;
                            prefix = "";
                            postfix = "";
                        }
                    }
                } break;
                default: {
                    if (!pos) {
                        if (completed) {
                            file.close();
                            throw std::runtime_error("[rgi] redundant specifier");
                        } else {
                            prefix += ch;
                        }
                    } else {
                        postfix += ch;
                        input = true;
                    }
                } break;
            }
        }
        file.close();
        std::cout << "\tName: \"" << name << "\", description: \"" << description << "\", version: " << version << ", author: \"" << author << "\"" << std::endl;
    }

    void Game::loadFromFile(const std::filesystem::path &rgv, const std::filesystem::path &rgd) {
        //rgv
        bool pos = false;
        bool completed = false;
        char ch;
        std::string prefix;
        std::string postfix;
        std::fstream file(rgv, std::ios::in | std::ios::binary);
        while (file.get(ch)) {
            switch (ch) {
                case ' ':{
                    if (!pos) {
                        if (!prefix.empty()){
                            completed = true;
                        }
                    } else {
                        if (!postfix.empty()) {
                            completed = true;
                        }
                    }
                } break;
                case '=':{
                    if (!pos) {
                        if (prefix.empty()) {
                            file.close();
                            throw std::runtime_error("[rgv] prefix not specified");
                        }
                        pos = true;
                        completed = false;
                    } else {
                        file.close();
                        throw std::runtime_error("[rgv] double '='");
                    }
                } break;
                case '\n':{
                    if (!pos) {
                        if (!prefix.empty()){
                            file.close();
                            throw std::runtime_error("[rgv] postfix not specified");
                        }
                    } else {
                        if (postfix.empty()) {
                            file.close();
                            throw std::runtime_error("[rgv] postfix not specified");
                        } else {
                            if (postfix == "true") {
                                boolVars.emplace(prefix, true);
                            } else if (postfix == "false") {
                                boolVars.emplace(prefix, false);
                            } else {
                                try {
                                    intVars.emplace(prefix, std::stoi(postfix));
                                } catch(...) {
                                    file.close();
                                    throw std::runtime_error("[rgv] postfix of int variable in't int");
                                }
                            }
                            pos = false;
                            completed = false;
                            prefix = "";
                            postfix = "";
                        }
                    }
                } break;
                default:{
                    if (!pos) {
                        if (completed) {
                            file.close();
                            throw std::runtime_error("[rgv] redundant specifier");
                        } else {
                            prefix += ch;
                        }
                    } else {
                        if (completed) {
                            file.close();
                            throw std::runtime_error("[rgv] redundant specifier");
                        } else {
                            postfix += ch;
                        }
                    }
                } break;
            }
        }
        file.close();
        std::cout << "\tVars:";
        for (const std::pair<std::string, bool> &var: boolVars) {
            std::cout << ' ' << var.first << '=' << (var.second ? "true" : "false");
        }
        for (const std::pair<std::string, short int> &var: intVars) {
            std::cout << ' ' << var.first << '=' << var.second;
        }
        std::cout << std::endl;
        //rgd
        std::string blockName;
        enum {Text, Choices, Vars} blockType = Text;
        bool inTag = false;
        completed = false;
        std::string tag;
        std::string text;
        prefix = "";
        postfix = "";
        pos = false;
        file.open(rgd, std::ios::in | std::ios::binary);
        //For backslash
        bool escaped = false;
        enum varsOperators {Set, Plus, Minus} varsOperator;
        //Need to be filled after operations with vector of games completed
        std::map<std::string, GameBlock*> gameBlockIDMap;
        std::map<std::string, bool> actionBoolVars;
        std::vector<std::tuple<std::string, short int, varsOperators>> actionIntVars;
        std::function<void(std::map<std::string, bool> &boolVars, std::map<std::string, short int> &intVars)> action;

        enum choicesOperators {Equals, NotEquals, Bigger, Lower} choicesOperator;
        std::vector<std::tuple<std::string, bool, choicesOperators>> choicesBoolVars;
        std::vector<std::tuple<std::string, short int, choicesOperators>> choicesIntVars;
        //parent gameblock, block to, text, condition
        std::vector<std::tuple<std::string, std::string, std::string, std::optional<std::function<bool(const std::map<std::string, bool> &boolVars, const std::map<std::string, short int> &intVars)>>>> gameBlockChoices;
        bool hasAction = false;
        bool inCondition = false;
        bool input = false;
        auto addChar = [&]{
            if (inTag) {
                tag += ch;
            } else {
                switch (blockType) {
                    case Text:{
                        text += ch;
                    } break;
                    case Vars:{
                        if (pos) {
                            postfix += ch;
                        } else {
                            prefix += ch;
                        }
                    } break;
                    /*
                    case Choices:{
                        if (pos) {
                            postfix += ch;
                        } else {
                            prefix += ch;
                        }
                    } break;*/
                }
            }
            input = true;
        };

        while (file.get(ch)) {
            switch (ch) {
                case '[':{
                    if (escaped) {
                        addChar();
                        escaped = false;
                        break;
                    }
                    input = false;
                    completed = false;

                    if (inTag) {
                        throw std::runtime_error("[rgd] double opened tag");
                    } else {
                        inTag = true;
                    }

                    //Vars block ending
                    if (blockType == Vars) {
                        //action func
                        if (!pos && !prefix.empty() || pos && !postfix.empty()) {
                            file.close();
                            throw std::runtime_error("[rgd] you must close vars block with end-line");
                        }
                        if (!actionBoolVars.empty() || !actionIntVars.empty()) {
                            action = [=](std::map<std::string, bool> &boolVars, std::map<std::string, short int> &intVars){
                                for (const auto &actionBoolVar: actionBoolVars){
                                    boolVars[actionBoolVar.first] = actionBoolVar.second;
                                }
                                for (const auto &actionIntVar: actionIntVars) {
                                    switch (std::get<2>(actionIntVar)) {
                                        case Set:{
                                            intVars[std::get<0>(actionIntVar)] = std::get<1>(actionIntVar);
                                        } break;
                                        case Plus:{
                                            intVars[std::get<0>(actionIntVar)] += std::get<1>(actionIntVar);
                                        } break;
                                        case Minus:{
                                            intVars[std::get<0>(actionIntVar)] -= std::get<1>(actionIntVar);
                                        } break;
                                    }
                                }
                            };
                            hasAction = true;
                        }
                    } else if (blockType == Choices) {
                        if (inCondition) {
                            file.close();
                            throw std::runtime_error("[rgd] condition not closed");
                        }
                        if (!pos && !prefix.empty() || pos && !postfix.empty()) {
                            file.close();
                            throw std::runtime_error("[rgd] you must close choices block with end-line");
                        }
                    }
                } break;
                case ']':{
                    if (escaped) {
                        addChar();
                        escaped = false;
                        break;
                    }
                    input = false;
                    completed = false;

                    if (inTag) {
                        if (tag.empty()) {
                            file.close();
                            throw std::runtime_error("[rgd] empty tag");
                        } else {
                            if (tag == "?") {
                                blockType = Choices;
                            } else if (tag == "#") {
                                blockType = Vars;
                            } else if (tag[0] == '/' && !blockName.empty()) {
                                if (tag.substr(1) == blockName) {
                                    while (!text.empty() && (text.back() == ' ' || text.back() == '\n')) {
                                        text = text.substr(0,text.size() - 1);
                                    }
                                    if (gameBlockIDMap.find(blockName) != gameBlockIDMap.end()) {
                                        file.close();
                                        throw std::runtime_error("[rgd] redefinition of block");
                                    }
                                    //blockType = Clear;
                                    if (hasAction) {
                                        gameBlocks.emplace_back(blockName, text, action);
                                    } else {
                                        gameBlocks.emplace_back(blockName, text);
                                    }
                                    text = "";
                                    blockName = "";
                                    hasAction = false;
                                    actionBoolVars.clear();
                                    actionIntVars.clear();
                                    blockType = Text;
                                } else {
                                    file.close();
                                    throw std::runtime_error("[rgd] invalid close tag");
                                }
                            } else {
                                if (!blockName.empty()) {
                                    file.close();
                                    throw std::runtime_error("[rgd] previous block not closed");
                                }
                                blockName = tag;
                                blockType = Text;
                            }
                        }
                        inTag = false;
                        tag = "";
                    } else {
                        file.close();
                        throw std::runtime_error("[rgd] redundant tag closing");
                    }
                } break;
                /*case '(':{

                } break;
                case ')':{

                } break;
                case ',':{

                } break;*/
                case '\n':{
                    if (inTag) {
                        file.close();
                        throw std::runtime_error("[rgd] tag need to be closed on single line");
                    }
                    if (blockType == Vars) {
                        if (!pos && !prefix.empty()) {
                            file.close();
                            throw std::runtime_error("[rgd] missing operator");
                        }
                        if (pos) {
                            if (postfix.empty()) {
                                file.close();
                                throw std::runtime_error("[rgd] postfix is empty");
                            }
                            if ((varsOperator == Plus || varsOperator == Minus) && (postfix == "false" || postfix == "true")) {
                                file.close();
                                throw std::runtime_error("[rgd] wrong value");
                            }
                            if (postfix == "false") {
                                actionBoolVars.emplace(prefix, false);
                            } else if (postfix == "true") {
                                actionBoolVars.emplace(prefix, true);
                            } else {
                                try {
                                    int intPostfix = std::stoi(postfix);
                                    actionIntVars.emplace_back(prefix, intPostfix, varsOperator);
                                } catch (...) {
                                    file.close();
                                    throw std::runtime_error("[rgd] error during value parsing");
                                }
                            }
                            pos = false;
                        }
                        completed = false;
                        input = false;
                        prefix = "";
                        postfix = "";
                    } else if (blockType == Text) {
                        if (input) {
                            addChar();
                        }
                    } else if (blockType == Choices) {

                    }
                } break;
                case '+':
                case '-':
                case '=':{
                    if (!inTag && blockType == Vars){
                        if (!pos) {
                            if (prefix.empty()) {
                                file.close();
                                throw std::runtime_error("[rgd] empty prefix in Vars");
                            }
                            switch (ch) {
                                case '=':{
                                    if (boolVars.find(prefix) == boolVars.end() && intVars.find(prefix) == intVars.end()) {
                                        file.close();
                                        throw std::runtime_error("[rgd] var does not exist");
                                    }
                                    varsOperator = Set;
                                } break;
                                case '-':{
                                    if (intVars.find(prefix) == intVars.end()) {
                                        file.close();
                                        throw std::runtime_error("[rgd] var does not exist");
                                    }
                                    varsOperator = Plus;
                                } break;
                                case '+':{
                                    if (intVars.find(prefix) == intVars.end()) {
                                        file.close();
                                        throw std::runtime_error("[rgd] var does not exist");
                                    }
                                    varsOperator = Minus;
                                } break;
                            }
                            pos = true;
                            completed = false;
                            input = false;
                        } else {
                            file.close();
                            throw std::runtime_error("[rgd] double operator in Vars");
                        }
                    } else {
                        if (completed) {
                            file.close();
                            throw std::runtime_error("[rgd] redundant specifier");
                        }
                        addChar();
                    }
                } break;
                case '\\':{
                    if (!escaped) {
                        escaped = true;
                    } else {
                        addChar();
                    }
                    input = true;
                } break;
                case ' ':{
                    if (escaped) {
                        addChar();
                    }
                    if (!completed) {
                        if (inTag) {
                            if (!tag.empty()) {
                                completed = true;
                            }
                        } else {
                            switch (blockType) {
                                case Text:{
                                    if (input) {
                                        addChar();
                                    }
                                } break;
                                case Vars:{
                                    if (!prefix.empty() && !pos) {
                                        completed = true;
                                    }
                                    if (!postfix.empty() && pos) {
                                        completed = true;
                                    }
                                } break;
                            }
                        }
                    }
                } break;
                default:{
                    if (completed) {
                        file.close();
                        throw std::runtime_error("[rgd] redundant specifier");
                    }
                    addChar();
                    escaped = false;
                } break;
            }
        }
        file.close();
        if (!blockName.empty()) {
            throw std::runtime_error("[rgd] block not closed");
        }
        for (auto &block: gameBlocks) {
            gameBlockIDMap.try_emplace(block.viewID(), &block);
        }
        //start block
        if (gameBlockIDMap.find("start") == gameBlockIDMap.end()) {
            throw std::runtime_error("[rgd] there is need to be \"start\" block");
        } else {
            this->startBlock = gameBlockIDMap["start"];
        }

        std::cout << "\tBlocks:\n";
        for (const GameBlock &block: gameBlocks) {
            std::cout << block.viewID() << '{' << block.viewText() << "}\n";
        }
    }

    GameBlock *Game::getGameBlockByID(const std::string &id) {
        for (GameBlock &gameBlock: gameBlocks) {
            if (gameBlock.viewID() == id) {
                return &gameBlock;
            }
        }
        return nullptr;
    }

    GameBlock *Game::getStartBlock() {
        return startBlock;
    }

    std::string Game::getName() const {
        return name;
    }

    const std::string &Game::viewName() const {
        return name;
    }

    std::string Game::getID() const {
        return id;
    }

    const std::string &Game::viewID() const {
        return id;
    }

    short int Game::getVersion() const {
        return version;
    }

    const short int &Game::viewVersion() const {
        return version;
    }

    std::string Game::getDescription() const {
        return description;
    }

    const std::string &Game::viewDescription() const {
        return description;
    }

    std::string Game::getAuthor() const {
        return author;
    }

    const std::string &Game::viewAuthor() const {
        return author;
    }

    const std::map<std::string, bool> &Game::viewBoolVars() const {
        return boolVars;
    }

    const std::map<std::string, short int> &Game::viewIntVars() const {
        return intVars;
    }
}