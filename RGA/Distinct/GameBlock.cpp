#include "GameBlock.h"

namespace RGA {
    GameBlock::GameBlock(const std::string &id, const std::string &text, const std::optional<std::function<void(std::map<std::string, bool> &boolVars, std::map<std::string, short int> &intVars)>> &action) {
        this->id = id;
        this->text = text;
        this->action = action;
    }

    std::string GameBlock::getText() const {
        return text;
    }

    const std::string &GameBlock::viewText() const {
        return text;
    }

    std::vector<GameBlockChoice> GameBlock::getChoices() const {
        return choices;
    }

    const std::vector<GameBlockChoice> &GameBlock::viewChoices() const {
        return choices;
    }

    std::string GameBlock::getID() const {
        return id;
    }

    const std::string &GameBlock::viewID() const {
        return id;
    }

    void GameBlock::execAction(std::map<std::string, bool> &boolVars, std::map<std::string, short int> &intVars) {
        action.value()(boolVars, intVars);
    }

    bool GameBlock::hasAction() {
        return action.has_value();
    }
}