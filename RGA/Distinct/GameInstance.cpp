#include "GameInstance.h"

RGA::GameInstance::GameInstance(RGA::Game *game, RGA::GameBlock *currentBlock) {
    this->game = game;
    this->currentBlock = currentBlock;
}

RGA::GameInstance::GameInstance(RGA::Game *game, RGA::GameBlock *currentBlock, const std::map<std::string, bool> &boolVars, const std::map<std::string, short int> &intVars) {
    this->game = game;
    this->currentBlock = currentBlock;
    this->boolVars = boolVars;
    this->intVars = intVars;
}


RGA::Game *RGA::GameInstance::getGame() {
    return game;
}

const std::string &RGA::GameInstance::viewCurrentBlockText() {
    return currentBlock->viewText();
}

