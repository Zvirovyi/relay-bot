template<class... Args>
void GameBlock::emplaceChoice(Args &&... args) {
    choices.emplace_back(std::forward<Args>(args)...);
}