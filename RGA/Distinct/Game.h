#pragma once

#include "GameBlock.h"
#include <string>
#include <vector>
#include <map>
#include <filesystem>
#include <iostream>
#include <fstream>
#include <functional>

namespace RGA {
    class Game{
    private:
        std::vector<GameBlock> gameBlocks;
        std::string id;
        std::string name;
        std::string description;
        std::string author;
        short int version;
        GameBlock *startBlock;
        std::map<std::string, bool> boolVars;
        std::map<std::string, short int> intVars;
    public:
        Game(const std::string &id, const short int &version, const std::string &name, const std::string &description, const std::string &author);

        Game(const std::string &id, const std::filesystem::path &rgi);

        void loadFromFile(const std::filesystem::path &rgv, const std::filesystem::path &rgd);

        [[nodiscard]]
        GameBlock *getGameBlockByID(const std::string &id);

        [[nodiscard]]
        GameBlock *getStartBlock();

        [[nodiscard]]
        std::string getName() const;

        [[nodiscard]]
        const std::string &viewName() const;

        [[nodiscard]]
        std::string getID() const;

        [[nodiscard]]
        const std::string &viewID() const;

        [[nodiscard]]
        short int getVersion() const;

        [[nodiscard]]
        const short int &viewVersion() const;

        [[nodiscard]]
        std::string getDescription() const;

        [[nodiscard]]
        const std::string &viewDescription() const;

        [[nodiscard]]
        std::string getAuthor() const;

        [[nodiscard]]
        const std::string &viewAuthor() const;

        [[nodiscard]]
        const std::map<std::string, bool> &viewBoolVars() const;

        [[nodiscard]]
        const std::map<std::string, short int> &viewIntVars() const;
    };
}