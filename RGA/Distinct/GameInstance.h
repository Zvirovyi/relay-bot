#pragma once

#include "Game.h"
#include <map>

namespace RGA {
    class GameInstance{
    protected:
        Game *game;
        GameBlock *currentBlock;
        std::map<std::string, bool> boolVars;
        std::map<std::string, short int> intVars;
    public:
        GameInstance(Game *game, GameBlock *currentBlock);
        GameInstance(Game *game, GameBlock *currentBlock, const std::map<std::string, bool> &boolVars, const std::map<std::string, short int> &intVars);
        Game *getGame();
        const std::string &viewCurrentBlockText();
    };
}