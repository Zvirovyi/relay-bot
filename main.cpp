#include <iostream>
#include "RGA/RGA.h"
#include "RelayBot.h"
#include <tgbot/tgbot.h>
#include <vector>

int main() {
    RB::GameSubsystem::connectDB();
    RB::GameSubsystem::loadGames();
    std::vector<RGA::Game *> games = RB::GameSubsystem::getGamesList();
    TgBot::Bot bot("817394417:AAHK8QWHA7gw4C4vj9mh5LnYr3f8FNOD38M");


    //Keyboard with games. Click opens additional game info menu
    TgBot::InlineKeyboardMarkup::Ptr keyboardAbout(new TgBot::InlineKeyboardMarkup);
    for (RGA::Game *game: games) {
        //Emplace new row
        keyboardAbout->inlineKeyboard.emplace_back();
        //Emplace button in row
        keyboardAbout->inlineKeyboard.back().emplace_back(new TgBot::InlineKeyboardButton);
        keyboardAbout->inlineKeyboard.back().back()->text = game->viewName();
        keyboardAbout->inlineKeyboard.back().back()->callbackData = "about" + game->viewID();
    }

    //Keyboard in additional game info menu to back to list or start this game
    TgBot::InlineKeyboardMarkup::Ptr keyboardStartSBackToList(new TgBot::InlineKeyboardMarkup);
    keyboardStartSBackToList->inlineKeyboard.emplace_back();
    keyboardStartSBackToList->inlineKeyboard.back().emplace_back(new TgBot::InlineKeyboardButton);
    keyboardStartSBackToList->inlineKeyboard.back().back()->text = "Начать эту игру";

    //Start button for fast interacting
    TgBot::InlineKeyboardButton::Ptr keyboardStartSBackToList_StartButton = keyboardStartSBackToList->inlineKeyboard.back().back();
    keyboardStartSBackToList->inlineKeyboard.back().emplace_back(new TgBot::InlineKeyboardButton);
    keyboardStartSBackToList->inlineKeyboard.back().back()->text = "Назад";
    keyboardStartSBackToList->inlineKeyboard.back().back()->callbackData = "backtolist";

    //Keyboard to only back to list
    TgBot::InlineKeyboardMarkup::Ptr keyboardBackToList(new TgBot::InlineKeyboardMarkup);
    keyboardBackToList->inlineKeyboard.emplace_back();
    keyboardBackToList->inlineKeyboard.back().emplace_back(new TgBot::InlineKeyboardButton);
    keyboardBackToList->inlineKeyboard.back().back()->text = "Назад";
    keyboardBackToList->inlineKeyboard.back().back()->callbackData = "backtolist";


    bot.getEvents().onCommand("start", [&](const TgBot::Message::Ptr &message) {
        bot.getApi().sendMessage(message->chat->id,"Приветствую! Сначала вы должны выбрать заинтересовавшую вас игру с помощью комманды /listgames.");
    });


    bot.getEvents().onCommand("listgames", [&](const TgBot::Message::Ptr &message) {
        std::optional<RGA::Game *> instanceGame = RB::GameSubsystem::getInstanceGame(message->chat->id);
        std::string res;
        if (instanceGame.has_value()){
            if (instanceGame.value() == nullptr) {
                res += "Ваше сохранение было уничтожено, потому что игра претерпела существенных изменений, либо её удалили :)\n\n";
            } else {
                res += "Вы сейчас в игре: " + instanceGame.value()->viewName() + "\nОстановить её можно с помощью команды /stopgame.\n\n";
            }
        }
        res += "Узнать подробности об игре можно, кликнув на неё.\n\n";
        res += "Игры:";
        bot.getApi().sendMessage(message->chat->id, res, false, 0, keyboardAbout);
    });


    bot.getEvents().onCallbackQuery([&](const TgBot::CallbackQuery::Ptr &query) {
        if (query->data.starts_with("about")) {
            std::string gameID = query->data.substr(5);
            RGA::Game *game = RB::GameSubsystem::getGameByID(gameID);
            if (game != nullptr) {
                std::optional<RGA::Game *> instanceGame = RB::GameSubsystem::getInstanceGame(query->message->chat->id);
                std::string res = std::string("Название: ") + game->viewName() + "\nОписание: " + game->viewDescription() + "\nАвтор: " + game->viewAuthor();
                if (!instanceGame.has_value() || instanceGame.value() == nullptr) {
                    //Not in game
                    keyboardStartSBackToList_StartButton->callbackData = "start" + gameID;
                    bot.getApi().editMessageText(res, query->message->chat->id, query->message->messageId, query->inlineMessageId, "", false, keyboardStartSBackToList);
                } else {
                    if (game == instanceGame.value()) {
                        bot.getApi().editMessageText("Вы сейчас в этой игре.\n\n" + res, query->message->chat->id, query->message->messageId, query->inlineMessageId, "", false, keyboardBackToList);
                    } else {
                        bot.getApi().editMessageText(res, query->message->chat->id, query->message->messageId, query->inlineMessageId, "", false, keyboardBackToList);
                    }
                }
            } else {
                bot.getApi().editMessageText("Такой игры не существует :(", query->message->chat->id, query->message->messageId, query->inlineMessageId, "", false, keyboardBackToList);
            }
        } else if (query->data == "backtolist") {
            //copypastttt
            std::optional<RGA::Game *> instanceGame = RB::GameSubsystem::getInstanceGame(query->message->chat->id);
            std::string res;
            if (instanceGame.has_value()){
                if (instanceGame.value() == nullptr) {
                    res += "Ваше сохранение было уничтожено, потому что игра претерпела существенных изменений, либо её удалили :)\n\n";
                } else {
                    res += "Вы сейчас в игре: " + instanceGame.value()->viewName() + ".\nОстановить её можно с помощью команды /stopgame.\n\n";
                }
            }
            res += "Узнать подробности об игре можно, кликнув на неё.\n\n";
            res += "Игры:";
            bot.getApi().editMessageText(res, query->message->chat->id, query->message->messageId, query->inlineMessageId, "", false, keyboardAbout);
        } else if (query->data.starts_with("start")) {
            if (RB::GameSubsystem::inGame(query->message->chat->id)) {
                bot.getApi().sendMessage(query->message->chat->id, "Ну, зачем тебе начинать несколько игр одновременно? Сначала нужно остановить текущую игру с помощью команды /stopgame.");
            } else {
                std::string gameID = query->data.substr(5);
                if (RB::GameSubsystem::getGameByID(gameID) != nullptr){
                    RB::GameSubsystem::startGame(RB::GameSubsystem::getGameByID(gameID), query->message->chat->id);
                    bot.getApi().deleteMessage(query->message->chat->id, query->message->messageId);
                    bot.getApi().sendMessage(query->message->chat->id, RB::GameSubsystem::viewInstanceText(query->message->chat->id));
                } else {
                    bot.getApi().sendMessage(query->message->chat->id, "Такой игры уже не существует :(");
                }
            }
        }
    });


    bot.getEvents().onCommand("stopgame", [&](const TgBot::Message::Ptr &message) {
        RB::GameSubsystem::stopGame(message->chat->id);
        bot.getApi().sendMessage(message->chat->id, "Если и была какая-то начатая игра, то сейчас её уже точно нету. Выбрать себе дальнейшее развлечение можно с помощью команды /listgames.");
    });


    TgBot::TgLongPoll longPoll(bot);

    std::cout << "Starting bot..." << std::endl;
    while(true) {
        try {
            longPoll.start();
        } catch (TgBot::TgException &e) {
            std::cout << "TgBot error: " << e.what() << std::endl;
        }
    }
    return 0;
}